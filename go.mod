module gitlab.com/sparetimecoders/goamqp

go 1.14

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/google/uuid v1.1.1
	github.com/pkg/errors v0.9.1
	github.com/streadway/amqp v0.0.0-20190827072141-edfb9018d271
	github.com/stretchr/testify v1.3.0
)
